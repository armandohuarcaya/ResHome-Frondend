import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/404.vue'

// Import Views - Dash
import DashboardView from './components/views/Dashboard.vue'
import TablesView from './components/views/Tables.vue'
import TasksView from './components/views/Tasks.vue'
import SettingView from './components/views/Setting.vue'
import ServerView from './components/views/Server.vue'
import ReposView from './components/views/Repos.vue'
import MonitoreoView from './components/views/Monitoreo.vue'
import FirebaseView from './components/views/Firebase.vue'
import PruebaView from './components/views/Prueba.vue'

// Routes
const routes = [
  {
    path: '/login',
    component: LoginView
  },
  {
    path: '/',
    component: DashView,
    children: [
      {
        path: 'dashboard',
        alias: '',
        component: DashboardView,
        name: 'Dashboard',
        meta: {description: 'Overview of environment'}
      }, {
        path: 'tables',
        component: TablesView,
        name: 'Tables',
        meta: {description: 'Simple and advance table in CoPilot'}
      }, {
        path: 'tasks',
        component: TasksView,
        name: 'Tasks',
        meta: {description: 'Tasks page in the form of a timeline'}
      }, {
        path: 'setting',
        component: SettingView,
        name: 'Settings',
        meta: {description: 'User settings page'}
      },
      {
        path: 'server',
        component: ServerView,
        name: 'Servers',
        meta: {description: 'List of our servers', requiresAuth: true}
      }, {
        path: 'repos',
        component: ReposView,
        name: 'Repository',
        meta: {description: 'List of popular javascript repos'}
      },
      {
        path: 'monitoreo',
        component: MonitoreoView,
        name: 'Monitoreo',
        meta: {description: 'List of popular javascript repos'}
      },
      {
        path: 'firebase',
        component: FirebaseView,
        name: 'Firebase',
        meta: {description: 'List of popular javascript repos'}
      },
      {
        path: 'prueba',
        component: PruebaView,
        name: 'Prueba',
        meta: {description: 'List of popular javascript repos'}
      }
    ]
  },
  {
    // not found handler
    path: '*',
    component: NotFoundView
  }
]

export default routes
